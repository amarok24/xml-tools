#!/usr/bin/env php
<?php
/*
Expat (XML parser) demo.
Author: Jan Prazak
Website: https://codeberg.org/amarok24/xml-tools
Date: 2024-04-30
License: The Unlicense
*/

declare(strict_types=1);

define('FNAME', 'samples/accountdata.xml');
//define('FNAME', 'samples/bbc-world.xml');

$myparser; // XMLParser
$fr; // file pointer (read mode) resource | false
$textdata; // bool | string

$current_depth = 0;
$current_cdata = '';
$current_elname = '';

function proc_start_el(XMLParser $p, string $name, array $attr) : void
{
	global $current_depth, $current_elname;

	$current_elname = $name;

	printf('%*s[%s]%s', $current_depth * 2, ' ', $current_elname, PHP_EOL);
	$current_depth++;

	if (count($attr) && isset($attr['name']))
		printf('%*sname=%s%s', $current_depth * 2, ' ', $attr['name'], PHP_EOL);

	if ($current_elname === 'Transaction')
		print ("-=-= TRANSACTION DETAILS =-=-\n\n");
}

function proc_end_el(XMLParser $p, string $name) : void
{
	global $current_depth, $current_cdata;

	$cdata = trim($current_cdata);
	$current_cdata = '';

	if ($name === 'Transaction')
		print ("\n");

	if (strlen($cdata))
		printf('%*s"%s"%s', $current_depth * 2, ' ', $cdata, PHP_EOL);

	$current_depth--;
}

/*
Character data containing non-ASCII text will be split into multiple calls to the handler function!
Handler is called for every piece of a text in the XML document. It can be called multiple times inside each fragment (e.g. for non-ASCII strings).
*/
function proc_cdata(XMLParser $p, string $text) : void
{
	global $current_cdata;

	// if (!strlen(trim($text))) return;

	$current_cdata .= $text;
}

/*
	This handler will be called in special cases like an XML comment
	or <?xml-stylesheet... element.
*/
function proc_default(XMLParser $p, string $text) : void
{
	printf('!! Special string: "%s"%s', $text, PHP_EOL);
}

$myparser = xml_parser_create('UTF-8');
// Case folding (uppercase names) is enabled by default, so disable
xml_parser_set_option($myparser, XML_OPTION_CASE_FOLDING, false);

// Skip whitespace characters (false by default), but may not work(?)
//xml_parser_set_option($myparser, XML_OPTION_SKIP_WHITE, true);

xml_set_element_handler($myparser, 'proc_start_el', 'proc_end_el');
xml_set_character_data_handler($myparser, 'proc_cdata');

// Handle XML data that does not have a specific handler defined:
xml_set_default_handler($myparser, 'proc_default');

if (! ($fr = fopen(FNAME, 'r'))) {
	exit("could not open XML input\n");
}

$parse_success = 1;

while ($textdata = fread($fr, 4096)) {
	$parse_success = xml_parse($myparser, $textdata, feof($fr));

	if (! $parse_success) {
		exit(
			sprintf(
				"XML error: %s at line %d\n",
				xml_error_string(xml_get_error_code($myparser)),
				xml_get_current_line_number($myparser)
			)
		);
	}
}

print (PHP_EOL);

xml_parser_free($myparser);
fclose($fr);
